declare const Polymer: any;

interface HTMLPropertyList {
    [name: string]: any;
}

interface HTMLElement {
    is: string;
    properties: HTMLPropertyList;
}
