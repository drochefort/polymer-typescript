# Polymer Typescript Boilerplate #

Boilerplate for building custom Polymer elements with Typescript, Sass and modern tooling.

### How do I get set up? ###

```
#!bash

npm install -g bower vulcanize eslint typescript grunt-cli
npm install && bower install
npm start
```

### Technology stack

** Automation **

- Node (version 4 or above)
- Bower to manage Polymer dependency
- NPM for all other dependencies
- Grunt 1.0 task runner

**JavaScript**

- typescript syntax
- typescript-formatter to prettify the code
- tslint to lint the code
- typescript for static type checking
- rollup for module bundling
- uglify for minification

**CSS**

- sass syntax
- stylelint for linting sass against the stylelint [standard](https://github.com/stylelint/stylelint-config-standard) style guide
- PostCSS for transpiling into CSS
- Minify with cssnano

**HTML**

- HTML5
- Polymer 1.4