const load = require('load-grunt-tasks');
const json = require('rollup-plugin-json');
const uglify = require('rollup-plugin-uglify');
const typescript = require('rollup-plugin-typescript');
const scss = require('postcss-scss');
const stylelint = require('stylelint');
const precss = require('precss');
const sorting = require('postcss-sorting');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');

module.exports = (grunt) => {
    load(grunt);

    grunt.initConfig({
        eslint: {
            options: {
                configFile: '.eslintrc.json',
            },
            target: ['Gruntfile.js'],
        },
        tslint: {
            options: {
                configuration: grunt.file.readJSON('tslint.json'),
            },
            main: {
                src: ['src/**/*.ts'],
            },
        },
        'typescript-formatter': {
            files: ['src/**/*.ts'],
        },
        ts: {
            default: {
                options: {
                    noEmit: true,
                },
                src: ['src/**/*.ts'],
            },
        },
        rollup: {
            options: {
                format: 'iife',
                sourceMap: true,
                plugins: [json(), typescript(), uglify()],
            },
            app: {
                src: 'src/ts/index.ts',
                dest: 'src/build/index.js',
            },
        },
        postcss: {
            options: {
                syntax: scss,
            },
            lint: {
                options: {
                    writeDest: false,
                    map: false,
                    processors: [
                        stylelint({ configFile: './.stylelintrc' }),
                    ],
                },
                src: 'src/sass/**/*.scss',
            },
            compile: {
                options: {
                    map: {
                        inline: false, // save all sourcemaps as separate files...
                        annotation: 'src/build/', // ...to the specified directory
                    },
                    processors: [
                        precss,
                        sorting(),
                        autoprefixer({ browsers: 'last 2 versions' }),
                        cssnano(),
                    ],
                },
                src: 'src/sass/index.scss',
                dest: 'src/build/index.css',
            },
        },
        mkdir: {
            default: {
                options: {
                    create: ['elements'],
                },
            },
        },
        vulcanize: {
            default: {
                options: {
                    inlineCss: true,
                    inlineScripts: true,
                    stripComments: true,
                    excludes: [
                        './polymer/polymer.html',
                        './webcomponentsjs/webcomponents-lite.min.js',
                    ],
                },
                files: {
                    'elements/my-element.html': 'src/my-element.html',
                },
            },
        },
        watch: {
            app: {
                options: {
                    livereload: true,
                },
                tasks: ['build'],
                files: [
                    '*.html',
                    'src/*.html',
                    'src/ts/**/*.ts',
                    'src/sass/**/*.scss',
                ],
            },
        },
        connect: {
            app: {
                options: {
                    port: 9500,
                    livereload: true,
                    base: ['.'],
                },
            },
        },
        open: {
            app: {
                path: 'http://localhost:9500',
            },
        },
    });

    grunt.registerTask('build', [
        'typescript-formatter', // automatically beautify typescript code
        'tslint', // lint typescript code
        'ts', // static type checking
        'rollup', // bundle
        'postcss:lint',
        'postcss:compile',
        'mkdir',
        'vulcanize',
    ]);

    grunt.registerTask('serve', ['connect', 'open', 'watch']);

    grunt.registerTask('default', ['build', 'serve']);
};
